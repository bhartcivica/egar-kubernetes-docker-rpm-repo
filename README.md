# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

To hold the repo information for installing Kubernetes and Docker on Centos 7

### How do I get set up? ###

Download the .repo file directly from this location to the /etc/yum.repos.d folder on your Centos installation. Run 'yum update' prior to installing Docker and Kubernetes.

### Contribution guidelines ###

Run the following after installing the repo: 'yum -y install --enablerepo=virt7-docker-common-release kubernetes etcd flannel'

### Who do I talk to? ###

Contact: repo admin